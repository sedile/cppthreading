#include <mutex>

class DeadLock {
private:
    unsigned int value;
    std::mutex myMutexOne, myMutexTwo;
    void deadlockOne(int n, int tid);
    void deadlockTwo(int n, int tid);
public:
    explicit DeadLock();
    virtual ~DeadLock();
    void deadlock();
};
