#include <iostream>
#include <thread>

#include "DeadLock.h"

DeadLock::DeadLock() : value(0) {}

DeadLock::~DeadLock(){}

void DeadLock::deadlockOne(int n, int tid){
    std::cout << "Thread : " << tid << " startet" << "\n";
    for(int i = 0; i < n; i++){
        std::lock(myMutexOne, myMutexTwo);
        std::lock_guard<std::mutex> guard1(myMutexOne, std::adopt_lock);
        std::lock_guard<std::mutex> guard2(myMutexTwo, std::adopt_lock);
        //std::scoped_lock<std::mutex, std::mutex>(dlmutexOne, dlmutexTwo); (neuere Version)
        value++;
    }
    std::cout << "Thread : " << tid << " ist fertig" << "\n";
}

void DeadLock::deadlockTwo(int n, int tid){
    std::cout << "Thread : " << tid << " startet" << "\n";
    for(int i = 0; i < n; i++){
        std::lock(myMutexTwo, myMutexOne);
        std::lock_guard<std::mutex> guard2(myMutexTwo, std::adopt_lock);
        std::lock_guard<std::mutex> guard1(myMutexOne, std::adopt_lock);
        //std::scoped_lock<std::mutex, std::mutex>(dlmutexOne, dlmutexTwo); (neuere Version)
        value++;
    }
    std::cout << "Thread : " << tid << " ist fertig" << "\n";
}

/* Aufrufen : Wie man deadlocks verhindert */
void DeadLock::deadlock(){
    std::thread t1(&DeadLock::deadlockOne, this, 100, 1);
    std::thread t2(&DeadLock::deadlockTwo, this, 100, 2);
    t1.join();
    t2.join();

    std::cout << value << "\n";
}
